import { Checkbox, Input } from "antd";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import ListView from "../Gists/ListView";
import {getSingleGist} from '../Services/Service'

export function Search() {
  const [query,updateQuery] = useState();
  const [dataSource,setDataSource] = useState<any>([]);
  const [gist, setGist] = useState<any>({});
  const navigate = useNavigate();

    //state searchValue 
    function searchGist(event:any){
       updateQuery(event.target.value)
    }
    const searchGists = async()=>{
      let res:any = await getSingleGist(query);
      setDataSource([res.data]);
    }
    useEffect(()=>{
      if(query){
        searchGists();
      }
      console.log("parrent change");
    },[query]);
    const columns = [
      {
        title: "",
        dataIndex: "owner",
        render: (owner: { id: any }) => (
          <Checkbox onChange={getGistDetail} value={owner.id}></Checkbox>
        ),
      },
      {
        title: "Profile",
        dataIndex: "owner",
        render: (owner: { avatar_url: any }) => (
          <img src={owner.avatar_url} alt="abc" className="circleImg" />
        ),
      },
      {
        title: "Name",
        dataIndex: "owner",
        render: (owner: { login: any }) => `${owner.login}`,
      },
      {
        title: "Date",
        dataIndex: "created_at:",
        render: (created_at: string) => new Date(created_at),
      },
      {
        title: "Comments",
        dataIndex: "comments",
      },
    ];
    function getGistDetail(e: any) {
      if (e.target.checked) {
        for (let d of dataSource) {
          if (d.owner.id == e.target.value) {
            setGist(d);
            navigate("/gistDetails", { state: { gist: d } });
            return;
          }
        }
      }
    }
  return (
    <>
      <Input placeholder="Search Gists" onChange={searchGist}></Input>
      {query && <ListView data={dataSource} columns={columns}></ListView>} 
    </>
  );
}
