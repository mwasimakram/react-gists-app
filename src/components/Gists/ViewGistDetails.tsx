import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import axios from "axios";
import { Avatar, Card } from "antd";
import "./Gist.css";
import { fork } from "child_process";
import Meta from "antd/lib/card/Meta";
import { Row, Col } from "antd";

interface IProps {
  gist?: any;
}

export function ViewGistDetails(props: IProps) {
  const data = useLocation();
  console.log(data.state);
  const [fileData, setFileData] = useState("");
  const [fileName, setFileName] = useState("");
  const [profile, setProfile] = useState("");

  let gist: any = data.state;
  useEffect(() => {
    for (let [key, val] of Object.entries(gist.gist.files)) {
      let obj: any = val;
      axios
        .get(obj.raw_url)
        .then((data) => {
          console.log(data.data);
          setFileName(key);
          setFileData(data.data);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  });
  return (
    <Row>
      <Col span={2} ></Col>
      <Col span={20}>
        <Card className="card" style={{width:"auto"}}>
          <Meta
            avatar={
              <Avatar className="circleImg" src={gist.gist.owner.avatar_url} />
            }
            title={gist.gist.owner.login + " /" + fileName}
          />
          <p style={{ padding: "25px" }}>{fileData}</p>
        </Card>
      </Col>
      <Col span={2} ></Col>
    </Row>
  );
}
