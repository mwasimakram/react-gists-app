import { Button, Col, Form, Input, Radio, Row } from "antd";
import TextArea from "antd/lib/input/TextArea";
import {addGistFile} from "../Services/Service"
export function CreateNewFile() {
  const [form] = Form.useForm();
  const changeFormValue = () => {
    console.log(form.getFieldValue("description"));
  };
  const submitForm = async (value:any)=>{
    let body:any = {};
    body.description=value.description;
    body.files={[value.filename]:{'content':value.content}};
    let res = await addGistFile(body);
    console.log(res);
  }
  return (
    <Row>
      <Col span={2}></Col>
      <Col span={20}>
        <Row><h4>Add New File</h4></Row>
        <Form form={form} onValuesChange={changeFormValue} onFinish={submitForm}>
          <Form.Item
            name="description"
            rules={[{ required: false, message: "Enter Gist Discription" }]}
          >
            <Input placeholder="Enter Gist Description" />
          </Form.Item>

          <Form.Item
            name="filename"
            rules={[{ required: false, message: "Enter filename" }]}
          >
            <Input placeholder="Enter filename" />
          </Form.Item>
          <Form.Item name="content" rules={[{ required: true }]}>
            <TextArea rows={4} placeholder="File Content" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              Create
            </Button>
          </Form.Item>
        </Form>
      </Col>
      <Col span={2}></Col>
    </Row>
  );
}
