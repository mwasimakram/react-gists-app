import { Avatar, Card, Col, Layout, Row } from "antd";
import Meta from "antd/lib/card/Meta";
import { Content } from "antd/lib/layout/layout";
import axios from "axios";
import { useEffect, useState } from "react";
export function GistsGridView(props: any) {
  let fileData: any = "Lorum ipsum";
  const [gistsDetails, setGistDetails] = useState(props.data);
  // const [gists,setGist] = useState([]);
  gistsDetails.forEach((g: any) => {
    Object.keys(g.files).map(async (key, value: any) => {
      const data = await axios.get(g.files[key].raw_url);
      g.fileData = data.data;
    });
  });
  console.log(gistsDetails);
  useEffect(() => {
  
  });
  return (
    <Layout>
      <Content>
        <Row>
          {gistsDetails &&
            gistsDetails.length > 0 &&
            gistsDetails.map((g: any, index: any) => (
              <>
                <Col  style={{ marginBottom: "20px" }} span={7}>
                  <Card key={index}>
                    <p>{g.fileData}</p>
                    <Card>
                      <Meta
                        avatar={
                          <Avatar
                            className="circleImg"
                            src={g.owner.avatar_url}
                          />
                        }
                        title={g.owner.login}
                      />
                    </Card>
                  </Card>
                </Col>{" "}
                <Col span={1}></Col>
              </>
            ))}{" "}
        </Row>
      </Content>
    </Layout>
  );
}
