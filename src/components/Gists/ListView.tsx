import { Table, Switch, Button } from "antd";
import { configConsumerProps } from "antd/lib/config-provider";
import React, { useEffect, useRef } from "react";
import { useState } from "react";
import { GistsGridView } from "./GistsGridView";
import { Row, Col } from "antd";
import { serialize } from "v8";
interface IProps {
  data: [];
  columns: any;
}
function ListView(props: any) {
  // let [data,setData] = useState(props.data);
  let [data,setData] = useState(props.data);
  const [columns,updateColumns] = useState(props.columns);
  
  // useEffect(()=>{
  //   console.log("data change",data);
  //    setData(props.data);
  // },[props.data ]);
  return (
    // <Row>
    //   <Col span={24}>
    //   </Col>
    // </Row>
    <>
    <Table dataSource={data} columns={columns} />
    </>
    

  );
}
export default React.memo(ListView);
