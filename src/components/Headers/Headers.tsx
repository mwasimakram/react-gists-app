import { Layout, Input, Button } from "antd";
import { Header } from "antd/lib/layout/layout";
import { Link } from "react-router-dom";
import {redirectToGit} from '../Services/Service'
import "./Headers.css";
import {useAuth} from '../Auth/useAuth'
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { Search } from "../Search/Search";
import { loggedIn } from "../../reducer/UserAuth";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
export function Headers() {
  const {login,authed} = useAuth();
  const navigate = useNavigate();
  function redirectToGitHub(){
    redirectToGit();
  }
  const {isLoggin} = useSelector((state:any)=>state.userAuth);

  const dispatch = useDispatch();

  const handleLogin = async () => {
    let result =  await login();
    console.log("result",result);
    navigate('/')
    dispatch(loggedIn());
    console.log("isLoggedIn",isLoggin)
 
  };
 
  return (
    <Header>
    <div className="logo"></div>
    <div className="searchInput">
      {/* <a href="http://localhost:3001/"> */}
      <Button onClick={redirectToGitHub}>Login</Button>

      {/* </a> */}
    </div>
    <div className="searchInput">
      <Search />
    </div>
  </Header>
  );
}
