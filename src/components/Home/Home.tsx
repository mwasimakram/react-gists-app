import { render } from "@testing-library/react";
import { Table, Switch, Grid, Checkbox, Layout, Row } from "antd";
import { Content } from "antd/lib/layout/layout";
import { useEffect, useState } from "react";
import { GistsGridView } from "../Gists/GistsGridView";
import ListView from "../Gists/ListView";
import { GetList } from "../Services/Service";
import { ViewGistDetails } from "../Gists/ViewGistDetails";
import { Link, useNavigate } from "react-router-dom";
import { Headers } from "../Headers/Headers";

import "./Home.css";
export function Home() {
  const [dataSource, isSetDataSource] = useState<any>([]);
  const [gist, setGist] = useState<any>({});
  const navigate = useNavigate();
  const columns = [
    {
      title: "",
      dataIndex: "owner",
      render: (owner: { id: any }) => (
        <Checkbox onChange={getGistDetail} value={owner.id}></Checkbox>
      ),
    },
    {
      title: "Profile",
      dataIndex: "owner",
      render: (owner: { avatar_url: any }) => (
        <img src={owner.avatar_url} alt="abc" className="circleImg" />
      ),
    },
    {
      title: "Name",
      dataIndex: "owner",
      render: (owner: { login: any }) => `${owner.login}`,
    },
    {
      title: "Date",
      dataIndex: "created_at:",
      render: (created_at: string) => new Date(created_at),
    },
    {
      title: "Comments",
      dataIndex: "comments",
    },
  ];
  let [isListView, setListView] = useState(true);
  function getGistDetail(e: any) {
    if (e.target.checked) {
      for (let d of dataSource) {
        if (d.owner.id == e.target.value) {
          alert(e.target.value)
          setGist(d);
          navigate("/gistDetails", { state: { gist: d } });
          return;
        }
      }
    }
  }
  function changeView(checked: Boolean) {
    if (checked) {
      setListView(false);
    } else {
      setListView(true);
    }
  }
  useEffect(() => {
    async function GetGistsData() {
      let data = await GetList();
      isSetDataSource(data.data);
    }
    GetGistsData();
  }, []);
  return (
    <>
      <Switch
        checkedChildren="Grid View"
        unCheckedChildren="List View"
        onChange={changeView}
      />
      {isListView && <ListView columns={columns} data={dataSource} />}
      {!isListView && <GistsGridView  data={dataSource}/>}
    </>
  );
}
