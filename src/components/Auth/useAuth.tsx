import { LoginOutlined } from "@ant-design/icons";
import * as React from "react";
import { Navigate } from "react-router-dom";

const authContext = React.createContext<any>(false);

export function useAuth() {
  const [authed, setAuthed] = React.useState(false);
  React.useEffect(() => {
  }, [authed]);
  return {
    authed,
    login() {
      return new Promise((res: any) => {
        setAuthed(true);
        res();
      });
    },
    logout() {
      return new Promise((res: any) => {
        setAuthed(false);
        res();
      });
    },
  };
}

export function AuthProvider({ children }: any) {
  const auth = useAuth();

  return <authContext.Provider value={auth}>{children}</authContext.Provider>;
}

export default function AuthConsumer() {
  return React.useContext(authContext);
}
export function RequireAuth({ children }: any) {
  const { authed } = useAuth();
  console.log("is this",authed  )
  return authed === true ? children : <Navigate to="/" replace />;
}
