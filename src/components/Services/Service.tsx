import { GetReq,PostReq } from "./Axios";
import { apiPath } from "../../Config";
export async function GetList(){
    try {
        let result:any;
        result = await GetReq(`${apiPath.getGistsList}`);
        return result.data;
    } catch (error) {
        throw error;
    }
}
export async function redirectToGit() {
    try {
    //    let result = await GetReq(`${apiPath.authorizeGit}`);
    let result = window.open('http://localhost:3001','_blank')
    console.log(result);
    
    return result; 
    } catch (error) {
        throw error
    }
}
export async function getSingleGist(id:any){
    try {
        let result  = await GetReq(`https://api.github.com/gists/${id}`);
        return result;
    } catch (error) {
        throw error;
    }
}
export async function addGistFile(obj={}){
    try {
        let result = await PostReq(`https://api.github.com/gists`,obj)
    } catch (error) {
        throw error;
    }
}