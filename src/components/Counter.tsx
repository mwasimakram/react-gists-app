import { Button } from "antd";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { increment,decrement } from "../reducer/Couter";
export function Counter(){
    const {value} = useSelector((state:any)=>state.counter);
    const dispatch = useDispatch();
    return(<>
    <h1>{value}</h1>
    <Button onClick={()=>dispatch(increment())}>Increment</Button>
    </>)
}