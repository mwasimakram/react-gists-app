import { createSlice } from "@reduxjs/toolkit";

const initialState={
    isLoggin:false
};

export const userAuthSlice = createSlice({
    name:"userAuth",
    initialState,
    reducers:{
        loggedIn:(state)=>{
            state.isLoggin=true;
        }
    }
});

export const {loggedIn} = userAuthSlice.actions;
export default userAuthSlice.reducer;