import App from "../App";
import { Route, Routes } from "react-router-dom";
import { ViewGistDetails } from "../components/Gists/ViewGistDetails";
import { Home } from "../components/Home/Home";
import { RequireAuth } from "../components/Auth/useAuth";
import { CreateNewFile } from "../components/Gists/CreateNewFile";
import { Counter } from "../components/Counter";
import { useSelector } from "react-redux";
import { is } from "immer/dist/internal";
export function Router() {
  const {isLoggin} = useSelector((state:any)=>state.userAuth);
  return (
    <>
      <Routes>
        {/* <Route path="/" element={<App />} /> */}
        <Route path="/" element={<Home />} />
        {/* <Route path="/login" element={<Login />}/> */}
        {/* <Route
          path="/gistDetails"
          element={
            <RequireAuth>
              <ViewGistDetails />
            </RequireAuth>
          }
        /> */}
      <Route path="/gistDetails" element={<ViewGistDetails />} />

        <Route path="/addGist" element={< CreateNewFile/>} />
        <Route  path="/Counter" element={< Counter/>}/>
      </Routes>
    </>
  );
}
