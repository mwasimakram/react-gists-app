import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { Layout } from "antd";
import { Content } from "antd/lib/layout/layout";
import { Headers } from "./components/Headers/Headers";
import { Home } from "./components/Home/Home";
import { ViewGistDetails } from "./components/Gists/ViewGistDetails";
import { Router } from "../src/Routes/Routes";
function App() {
  return (
    <Layout>
      <Headers />
      <Content>
        <Router />
      </Content>
    </Layout>
  );
}

export default App;
